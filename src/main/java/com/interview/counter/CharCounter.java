package com.interview.counter;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CharCounter {

    private CharCounter() {
    }


    public static Map<Character, Integer> countWithStreams(String src) {
        Stream<Character> characterStream = src.chars()
                .mapToObj(e -> (char) e);

        Map<Character, Integer> frequency = characterStream.collect(
                Collectors.toMap(
                        k -> k,
                        v -> 1,
                        Integer::sum
                ));
        return frequency;
    }

    public static Map<Character, Integer> countWithHashMap(String src) {
        Map<Character, Integer> frequency = new HashMap<>();
        char[] srcCharsArray = src.toCharArray();
        for (char c : srcCharsArray) {
            frequency.put(c, 0);
        }
        for (char c : srcCharsArray) {
            Integer currentValue = frequency.get(c);
            frequency.put(c, ++currentValue);
        }
        return frequency;
    }

    public static Map<Character, Integer> countWithHashMapLess(String src) {
        Map<Character, Integer> frequency = new HashMap<>();
        for (char c : src.toCharArray()) {
            frequency.putIfAbsent(c, 0);
            frequency.computeIfPresent(c, (key, value) -> ++value);
        }
        return frequency;
    }

}
