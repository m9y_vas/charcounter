package com.test;


import com.interview.counter.CharCounter;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Map.entry;
import static org.hamcrest.MatcherAssert.assertThat;

public class CounterTest {

    private static final String INPUT = "abcdAaa";

    private static final Map<Character, Integer> EXPECTED_RESULT = Collections.unmodifiableMap(
            Map.ofEntries(
                    entry('a', 3),
                    entry('b', 1),
                    entry('c', 1),
                    entry('d', 1),
                    entry('A', 1)
            )
    );

    @Test
    public void testStream() {
        Map<Character, Integer> result = CharCounter.countWithStreams(INPUT);
        System.out.println(formatMap(result));

        assertThat("result not equals for Streams", result.equals(CounterTest.EXPECTED_RESULT));
    }

    @Test
    public void testHashMap() {
        Map<Character, Integer> result = CharCounter.countWithHashMap(INPUT);
        System.out.println(formatMap(result));

        assertThat("result not equals for HashMap", result.equals(CounterTest.EXPECTED_RESULT));
    }

    @Test
    public void testHashMapLess() {
        Map<Character, Integer> result = CharCounter.countWithHashMapLess(INPUT);
        System.out.println(formatMap(result));

        assertThat("result not equals for HashMapLess", result.equals(CounterTest.EXPECTED_RESULT));
    }

    private String formatMap(Map<Character, Integer> map) {
        return map.entrySet()
                .stream()
                .map(e -> String.format("%s -> %d", e.getKey(), e.getValue()))
                .collect(Collectors.joining(", "));

    }
}
